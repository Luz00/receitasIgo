Ingredientes

    1 e 1/2 xícara (chá) de açúcar
    1/2 xícara (chá) de água
    1 canela em pau
    2 latas de doce de leite cremoso (800g)
    2 latas de leite (use a lata de leite condensado vazia para medir)
    7 ovos

Modo de preparo

Em uma panela, coloque o açúcar com a água e a canela, e leve ao fogo 
médio até virar caramelo. Retire a canela e despeje em uma forma de buraco 
no meio de 22 cm de diâmetro. No liquidificador, bata o doce de leite com o 
leite e os ovos até homogeneizar. Despeje na forma com o caramelo e leve ao 
forno médio, preaquecido, em banho-maria, por 1 hora ou até firmar e dourar. 
Espere esfriar e leve à geladeira por 3 horas. Desenforme e sirva, se desejar, 
decorado com canela.